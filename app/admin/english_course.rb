ActiveAdmin.register EnglishCourse do
  permit_params :name, :description, :price, :image, :school_id
end
