ActiveAdmin.register User do
# See permitted parameters documentation:
# https://github.com/activeadmin/activeadmin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters
#
# permit_params :list, :of, :attributes, :on, :model
#
# or
#
# permit_params do
#   permitted = [:permitted, :attributes]
#   permitted << :other if params[:action] == 'create' && current_user.admin?
#   permitted
# end
  menu :priority => 4
  menu label: 'Customers'
  config.batch_actions = true

  filter :first_name
  filter :last_name
  filter :email
  scope :all, label: 'All Customers', default: true
  scope('Customers with Orders') { |user| user.joins(:orders).group('id').having('count(orders.id) > 0')}

  index as: :block do |user|
    div for: user do
      resource_selection_cell user

      # Customer
       table_for(user) do
         column('Customer Id') {|user| user.id}
         column('First Name') {|user| link_to "#{user.first_name}", admin_user_path(user.id) }
         column('Last Name') {|user| user.last_name }
         column('Address') {|user| user.address}
         column('City') {|user| user.city}
         column('Postal Code') {|user| user.postal_code}
         column('Province') {|user| user.province}
         column('Email') {|user| user.email}
         column('Registered') {|user| user.encrypted_password ? 'yes' : 'no' }
         #  column("Stripe") {|user| user.stripe_customer_id}

         # Order
         table_for(user.orders) do
           column('Order', :sortable => :id) {|order| link_to "##{order.id}", admin_order_path(order) }
           #  column("State")                   {|order| status_tag(order.order_status.name) }
           column('Date', :sortable => :checked_out_at){|order| pretty_format(order.created_at) }
           column('Total before taxes', :sortable => :total){|order| number_to_currency(order.total) }
           column('PST', :sortable => :pst_ratel){|order| order.pst_rate }
           column('GST', :sortable => :gst){|order| order.gst }
           column('HST', :sortable => :hst){|order| order.hst }
           column('Grand Total', :sortable => :grand_total){|order| number_to_currency(order.grand_total) }
           column('Status', :sortable => :status_id){|order| order.status}
           column('Products') { |order|

           # Line Item
           table_for (order.line_items) do
             column('Line Item', :sortable => :id) {|line_item| link_to "#{line_item.english_course.name}", admin_line_item_path(line_item)}
             column('Qty', :sortable => :id) {|line_item| link_to "#{line_item.quantity}", admin_line_item_path(line_item)}
             column('Price', :sortable => :id) {|line_item| link_to "#{line_item.price}", admin_line_item_path(line_item)}
            #  column('Total') do |line_item|
            #    line_item.price.to_f
            #  end
            # t = 0
            # order.line_items do |li|
            #   t += li.price
            # end
            # column('T', :sortable => :id) {|line_item| t}

            #



          end
        }
      # column("Grand Total")                   {|order| number_to_currency order.total }
      end
    end  #user table
   end
  end

# def sum_total(a)
#   t = 0
#   a.each do |x|
#     t += x.price
#   end
#   t
# end

sidebar "Order History", :only => :show do
    attributes_table_for customer do
      row("Total Orders") { user.orders.complete.count }
      row("Total Value") { number_to_currency user.orders.complete.sum(:total_price) }
    end
  end

end
