ActiveAdmin.register Contact do
  permit_params :title, :description, :phone, :email
  actions :all, except: [:new, :destroy]
end
