class CourseController < ApplicationController
  def index
    conditions = {}
    conditions[:school_id] = params[:school_id] if params[:school_id]
    query = params[:q].presence || '*'
    EnglishCourse.reindex
    @courses = EnglishCourse.search(
      query,
      page: params[:page], per_page: 25,
      where: conditions
    )
  end

  def show
    @course = EnglishCourse.find(params[:id])
  end
end
