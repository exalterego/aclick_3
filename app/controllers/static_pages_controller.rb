class StaticPagesController < ApplicationController
  def about
    @about = About.first
  end

  def contact
    @contact = Contact.first
  end

  def school
    @school_courses = EnglishCourse.joins(:school).where(schools: { name: 'EC' }).order(:name).page(params[:page]).per(3)
    # @school_courses = EnglishCourse.find(params[:id])
  end

  def ilac
    @school_courses = EnglishCourse.joins(:school).where(schools: { name: 'ILAC' }).order(:name).page(params[:page]).per(3)
  end

  def new
    @school_courses = EnglishCourse.where('created_at > ?', 5.days.ago).order(:name).page(params[:page]).per(3)
  end

  def sale
    @school_courses = EnglishCourse.where('price < ?', 350).order(:name).page(params[:page]).per(3)
  end
end
