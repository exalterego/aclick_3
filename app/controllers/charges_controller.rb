class ChargesController < ApplicationController
  def new
    if user_signed_in?
      @amount = session[:grand_total]
    else
      @amount = session[:total]
    end
  end

  def create
    # Amount in cents

    if user_signed_in?
      return @amount = session[:grand_total]
    else
      return @amount = session[:total]
    end

    Cart.destroy(session[:cart_id])
    session[:cart_id] = nil

    customer = Stripe::Customer.create(
      :email => params[:stripeEmail],
      :source => params[:stripeToken]
    )

    charge = Stripe::Charge.create(
      :customer    => customer.id,
      :amount      => (@amount.to_f * 100).to_i,
      :description => 'Rails Stripe customer',
      :currency    => 'usd'
    )
  rescue Stripe::CardError => e
    flash[:error] = e.message
    redirect_to new_charge_path
  end
end
