class Cart < ApplicationRecord
  has_many :line_items, dependent: :destroy

  def add_product(product)
    current_item = line_items.find_by(english_course_id: product.id)
    if current_item
      current_item.quantity += 1

    else
      current_item = line_items.build(english_course_id: product.id)
    end
    current_item
  end

  def remove_product(product)
    current_item = line_items.find_by(english_course_id: product.id)
    if current_item.quantity > 1
      current_item.quantity -= 1
    elsif current_item.quantity == 1
      current_item.destroy
    end
    current_item
  end

  def total_price
    line_items.to_a.sum { |item| item.total_price }
  end

  def grand_total(total, current_user)
    gst_rate = current_user.province.gst
    pst_rate = current_user.province.pst
    hst_rate = current_user.province.hst

    pst = (total * pst_rate).round(2)
    gst = (total * gst_rate).round(2)
    hst = (total * hst_rate).round(2)

    total + pst + gst + hst
  end

  def total_qty
    line_items.to_a.sum { |item| item.quantity }
  end

  def qty
    qty = 0
    @cart.line_items.each do |li|
      qty += li.quantity
    end
    qty
  end
end
