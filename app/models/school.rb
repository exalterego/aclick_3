class School < ApplicationRecord
  has_many :english_courses
  validates :name, presence: true
end
