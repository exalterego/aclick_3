class LineItem < ApplicationRecord
  belongs_to :english_course
  belongs_to :cart

  def total_price
    english_course.price * quantity
  end
end
