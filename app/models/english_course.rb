class EnglishCourse < ApplicationRecord
  belongs_to :school
  has_many :line_items
  before_destroy :ensure_not_referenced_by_any_line_item
  #
  searchkick
  def search_data
    {
      name: name,
      description: description,
      school_id: school_id
    }
  end
  #
  mount_uploader :image, ImageUploader
  #
  validates :name, presence: true
  validates :name, length: { minimum: 5 }
  validates :name, length: { maximum: 50 }
  validates :description, presence: true
  validates :description, length: { minimum: 20 }
  validates :description, length: { maximum: 500 }
  validates :price, presence: true
  validates :price, numericality: true
  validates :image, presence: true

  private

  # ensure that there are no line items referencing this product
  def ensure_not_referenced_by_any_line_item
    unless line_items.empty?
      errors.add(:base, 'Line Items present')
      throw :abort
    end
  end
end
