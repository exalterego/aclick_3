class User < ApplicationRecord
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  #
  devise :database_authenticatable, :registerable, :recoverable, :rememberable, :trackable, :validatable
  belongs_to :province
  has_many :orders, dependent: :destroy
  # does not work
  # devise :database_authenticatable, :validatable, password_length: 1..128

  def name
    first_name + ' ' + last_name
  end
end
