class AddSchoolRefToEnglishCourses < ActiveRecord::Migration[5.0]
  def change
    add_reference :english_courses, :school, foreign_key: true
  end
end
