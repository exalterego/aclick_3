class AddImageToEnglishCourses < ActiveRecord::Migration[5.0]
  def change
    add_column :english_courses, :image, :string
  end
end
