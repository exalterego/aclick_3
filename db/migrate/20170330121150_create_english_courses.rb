class CreateEnglishCourses < ActiveRecord::Migration[5.0]
  def change
    create_table :english_courses do |t|
      t.string :name
      t.string :description
      t.decimal :price

      t.timestamps
    end
  end
end
