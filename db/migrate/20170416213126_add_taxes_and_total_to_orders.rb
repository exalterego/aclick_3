class AddTaxesAndTotalToOrders < ActiveRecord::Migration[5.0]
  def change
    add_column :orders, :pst_rate, :decimal
    add_column :orders, :gst, :decimal
    add_column :orders, :hst, :decimal
    add_column :orders, :total, :decimal
    add_column :orders, :grand_total, :decimal
  end
end
