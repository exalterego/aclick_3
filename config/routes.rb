Rails.application.routes.draw do
  devise_for :users
  resources :line_items do
    get 'decrease', on: :member
    get 'increase', on: :member
  end

  resources :orders

  resources :carts

  resources :charges

  root to: 'course#index'

  get 'courses/:id', to: 'course#show', as: :course_item

  get '/courses/search/:q', to: 'course#search', as: :search

  get 'about', to: 'static_pages#about', as: :about

  get 'contact', to: 'static_pages#contact', as: :contact

  get 'ec', to: 'static_pages#school', as: :ec

  get 'ilac', to: 'static_pages#ilac', as: :ilac

  get 'new', to: 'static_pages#new', as: :new

  get 'sale', to: 'static_pages#sale', as: :sale

  # get 'schools/:id', to: 'static_pages#school', as: :school_item

  devise_for :admin_users, ActiveAdmin::Devise.config
  ActiveAdmin.routes(self)
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  # root 'application#hello'
end
